println """= 101 Groovy Script : que en mi local funcionan
Varios
{buildVersion}
:id: 101-groovy-scripts
:doctype: book
:producer: Asciidoctor
:keywords: Asciidoctor, samples, e-book, EPUB3, KF8, MOBI, Asciidoctor.js
:copyright: CC-BY-SA 3.0
// NOTE anthology adds support for an author per chapter; use book for a single author
:publication-type: anthology
:idprefix:
:idseparator: -
:front-cover-image: image:portada.jpg[Front Cover,1050,1600]
:toc: left
:toclevels: 4
:leveloffset: 1
:media: print
:source-highlighter: rouge
:pdf-stylesdir: theme
:pdf-style: custom
:autofit-option:

include::../../jbake/content/index.adoc[]
"""
files = [:]

void scanDir( File dir ){
    dir.listFiles().findAll{!it.isDirectory()}.sort{it.name}.each{ f->
        if( !f.name.endsWith('.adoc'))
            return
        if( ["index.adoc","index-en.adoc","about.adoc","about-en.adoc",'plantuml.adoc'].contains(f.name))
            return
        if( f.name.endsWith('-en.adoc'))
            return

        String when = f.text.split('\n')[2]
        if( when.startsWith("if")) println f.name
        List list = files[when] ?: []
        list.add f
        files[when] = list
    }
    dir.listFiles().findAll{it.isDirectory()}.sort{it.name}.each{ d ->
        if( ['slides','js'].contains(d.name))
            return
        scanDir(d)
    }
}

scanDir(new File("src/jbake/content/"))
files.values().flatten().each{ f->
    String text = f.text.split('\n').find{
        it.startsWith(":jbake-script:")
    } ?: ''

     println """<<<<
$text
include::../../jbake/content/${f.parentFile.name}/${f.name}[]"""
}
//files.keySet().each{ println it}