//tag::grapes[]
@Grapes([
        @Grab(group='org.apache.poi',  module='poi',            version='3.17'    ),
        @Grab(group='org.apache.poi',  module='poi-ooxml',      version='3.17'    ),
        @Grab(group='org.asciidoctor', module='asciidoctorj',   version='1.5.6'   ),
        @Grab(group='org.jruby',       module='jruby-complete', version='9.1.15.0')
])
//end::grapes[]

import org.apache.poi.xslf.usermodel.*
import org.apache.poi.xslf.usermodel.XMLSlideShow

import org.asciidoctor.SafeMode
import org.asciidoctor.OptionsBuilder
import org.asciidoctor.Asciidoctor.Factory


//tag::create[]
void createSlides(){
    asciidoctor = Factory.create()
    options = OptionsBuilder.options().
            templateDirs(new File('./asciidoctor-reveal.js/','templates')).
            backend('revealjs').
            inPlace(true).
            safe(SafeMode.UNSAFE).
            get()

    asciidoctor.convertFile(new File("slide.adoc"), options)
}
//end::create[]

//tag::load[]
void dumpRevealJS(){
    ["git", "clone", "https://github.com/hakimel/reveal.js.git"].execute()
    ["git", "clone", "https://github.com/asciidoctor/asciidoctor-reveal.js.git"].execute()
}
//end::load[]

// limpiamos si hubiera restos anteriores
new File('.').eachFileMatch(~/slide.adoc/) { file ->
    file.delete()
}

//tag::each[]
File file = new File("slide.adoc")
XMLSlideShow ppt = new XMLSlideShow(new FileInputStream('titulo.pptx'))

ppt.getSlides().each{slide->
    String text = ""
    String image=""
    slide.getShapes().each{shape->
        if (shape instanceof XSLFPictureShape ){
            if (shape.getPictureData()){
                new File(shape.getPictureData().getFileName()) << shape.getPictureData().getData()
                image = "image:${shape.getPictureData().getFileName()}[]"
            }
        }else{
            if (shape.text)
                text = shape.text
        }
    }

    file <<  """
== ${text}
${slide_c.image} 
"""
}
//end::each[]

dumpRevealJS()
createSlides()
println "Creado!!!"