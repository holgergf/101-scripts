//tag::dependencies[]
@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')

@Grab(group='org.asciidoctor', module='asciidoctorj', version='1.5.6')
@Grab(group='org.asciidoctor', module='asciidoctorj-pdf', version='1.5.0-alpha.16')
@Grab(group='org.jruby', module='jruby-complete', version='9.1.15.0')

import groovyx.net.http.*
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson

import org.asciidoctor.OptionsBuilder
import org.asciidoctor.AttributesBuilder
import org.asciidoctor.SafeMode
import org.asciidoctor.Asciidoctor.Factory
//end::dependencies[]

//tag::consumir[]
def http = configure {
    request.uri = 'https://www.koliseo.com/'    
    request.contentType = JSON[0]
    request.accept =  JSON[0]
}.get{
   request.uri.path='/events/commit-2018/r4p/5630471824211968/agenda'
}
//end::consumir[]

//tag::transformar[]
days =[:]
speackers =[:]

http.days.each{ day ->
   def slots = []
   day.tracks.each{ track ->
       track.slots.each{ slot ->
            if( ['TALK'].contains(slot.contents?.type) ){
                def talk = [
                        title: slot.contents.title,
                        day: "$day.name",
                        when: "$slot.start $slot.end",
                        slot: slot.id,
                        authors: slot.contents.authors.collect{ it.name }.join(',')
                ]
                def d = days[day.name] ?: [:]
                def w = d[talk.when] ?: []

                w.add talk
                d[talk.when]= w
                days[day.name]=d

                slot.contents.authors.each{ author ->
                         def speacker = speackers[author.name] ?: []
                         speacker.add talk
                         speackers[author.name] = speacker
               }
            }
       }
   }
}
//end::transformar[]

println "talks:"
days.each{ d->
	d.each{
		println it
	}
}
println "speackers:"
speackers.each{
	println it
}

//tag::asciidoctor[]
file = new File("commit2018.adoc")
file.write "= Commit 2018\n"
file << "Agenda\n\n\n"
file << ":chapter-label:\n"
file << "\n"

days.sort().each{ day ->
    file << "== $day.key\n"
    day.value.sort{it.key}.each{
	    file << "=== $it.key\n"
            it.value.each{
	    	file << "- $it.title ($it.authors)\n\n"
	    }
    }
}

file << "== Speackers\n\n"

speackers.sort{it.key}.each{
    file << "=== $it.key\n"
    it.value.each{
	file << "- $it.title\n"
        file << "  $it.day $it.when \n\n"
    }
    file << "\n"
}

asciidoctor = Factory.create();
attributes = AttributesBuilder.attributes(). 
            docType('book').
            tableOfContents(true).
            sectionNumbers(false).
            sourceHighlighter("coderay").
            get()

options = OptionsBuilder.options(). 
            backend('pdf').
            attributes(attributes).
            safe(SafeMode.UNSAFE).
            get()

asciidoctor.convertFile(new File("commit2018.adoc"), options)
//end::asciidoctor[]
