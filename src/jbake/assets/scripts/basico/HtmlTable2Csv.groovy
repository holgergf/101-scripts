@Grab('org.ccil.cowan.tagsoup:tagsoup:1.2.1')
import org.ccil.cowan.tagsoup.Parser

new File(args[1]).withWriter('UTF-8') { writer ->
    def slurper = new XmlSlurper(new Parser())
    def url = args[0]
    def htmlParser = slurper.parse(url)
    htmlParser.'**'.find {it.@id==args[3] }.tr.each{
        writer.writeLine it.td.collect{"$it".trim()}.join(';')
    }
}