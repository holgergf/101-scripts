//tag::dependencias[]
@GrabConfig(systemClassLoader=true)
@Grab('org.slf4j:slf4j-api:1.7.10')
@Grab('org.xerial:sqlite-jdbc:3.21.0')
@Grab('org.grails:grails-datastore-gorm-hibernate5:6.1.6.RELEASE')
@Grab('com.enigmabridge:hibernate4-sqlite-dialect:0.1.2')

import groovy.sql.Sql
import groovy.transform.ToString
import org.grails.orm.hibernate.HibernateDatastore
import grails.gorm.annotation.Entity
//end::dependencias[]

//tag::domain[]
@Entity
@ToString
class Libro {
    String codigo
    String titulo
    static constraints = {
        codigo unique:true
    }
}

@Entity
@ToString
class Biblioteca{
    String nombre
    static hasMany = [ libros : Libro]
}
//end::domain[]

//tag::config[]
Map getConfiguration(){
    [
            'dataSource.url'			:'jdbc:sqlite:example.db',
            'dataSource.drive'			:'org.sqlite.JDBC',
            'hibernate.hbm2ddl.auto'	: 'update',
            'hibernate.dialect'			:'com.enigmabridge.hibernate.dialect.SQLiteDialect'
    ]
}

HibernateDatastore initDatastore(){
    new HibernateDatastore( configuration, Biblioteca, Libro)
}
//end::config[]

//tag::biblio[]
void prepareBiblioteca() {
    Biblioteca.withTransaction {

        Biblioteca.list().each{ it.delete() }

        new Biblioteca(nombre: 'Biblioteca Nacional').save()
    }
}
//end::biblio[]

//tag::libros[]
void addLibros(){
    Biblioteca.withTransaction{
        Biblioteca b = Biblioteca.first()
        assert b

        b.addToLibros(codigo:'abc', titulo: 'libro 1')

        b.save()
    }
}
//end::libros[]

//tag::list[]
void list(){
    Biblioteca.withTransaction{
        assert Biblioteca.count()   //<1>

        def list = Biblioteca.list()    //<2>
        list.each{ Biblioteca b->
            println "Biblioteca $b.nombre ($b.id): "
            b.libros.each{ Libro l->    //<3>
                println "\t $l.codigo con titulo $l.titulo"
            }

        }
    }

    Biblioteca.withNewSession{
        Biblioteca b = Biblioteca.findByNombre('Biblioteca Nacional')   //<4>
        assert b
        println "Biblioteca encontrada $b"

        List list = Biblioteca.findAllByNombreLike('Bibli%')    //<5>
        assert list.size()

        List list2 = Biblioteca.withCriteria{   //<6>
            eq 'nombre', 'Biblioteca Nacional'
        }
        assert list2.size()
    }
}
//end::list[]

//tag::main[]
initDatastore()

prepareBiblioteca()

addLibros()

println '-'.multiply(20)

list()
println '-'.multiply(20)
//end::main[]
