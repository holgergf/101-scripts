@Grab('com.aestasit.infrastructure.sshoogr:sshoogr:0.9.25')
@GrabConfig(systemClassLoader=true)
import static com.aestasit.infrastructure.ssh.DefaultSsh.*

options.trustUnknownHosts = true

String server = "src_server"

def cli = new CliBuilder(usage: 'groovy CliBuilder.groovy -[husc]')
cli.with { (1)
    h(longOpt: 'help               ','Usage Information \n', required: false)
    u(longOpt: 'Actualización      ','-u version servidor ruta   -> Ejemplo: -u v1 localhost', required: false)
    s(longOpt: 'Copiado de ficheros','-s servidor origen destino -> Ejemplo: -s localhost /tmp  /home/docu', required: false)
    c(longOpt: 'Crear Imagen       ','-c servidor ruta           -> Ejemplo: -c ruta', required: false)
}

def options = cli.parse(args)

if (!options) {
    return
}
if (options.h) {
    cli.usage()
    return
}
if (options.u) {
    if (options.arguments().size() < 3) {
       cli.usage()
        return
    }
    updateVersion(options.arguments()[0],options.arguments()[1],options.arguments()[2])
}
if (options.s) {
    if (options.arguments().size() < 3) {
        cli.usage()
        return
    }
    scpDocuments(options.arguments()[0],options.arguments()[1],options.arguments()[2])
}
if (options.c) {
    if (options.arguments().size() < 3) {
        cli.usage()
        return
    }
    createImage(options.arguments()[0],options.arguments()[1])
}
def updateVersion(version,server,orig){
    //tag::version[]
    remoteSession("user:password@$server:22"){
        remoteFile("$orig/gradle.properties").text = "version=$version"
    }
  //end::version[]
}

def createImage(server,src){
    //tag::create[]
    remoteSession("user:password@$server:22") {
        exec "cd $src; ./gradlew build"
        exec "cd $src; ./gradlew pushDockerRegistry"
    }
    //end::create[]
}

def scpDocuments(server,orig,dest){
    //tag::put_dir[]
    remoteSession("user:password@$server:22") {
        scp{
            from { localDir orig}
            into { remoteDir dest}

        }
    }
    //end::put_dir[]
}