//tag::dependencies[]
@Grab('com.jameskleeh:excel-builder:0.4.2')

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import com.jameskleeh.excel.ExcelBuilder
//end::dependencies[]

//tag::arguments[]
def cli = new CliBuilder(usage: 'groovy ExcelI18n.groovy -[h] -action generateProperties/generateExcel filename.xls ')

cli.with { // <1>
    h(longOpt: 'help', 'Import/Export excel file to i18n properties files.', required: false)
    action('generateProperties generateExcel', required: true, args:1, argName:'action')
}

def options = cli.parse(args)
if (!options){
    return
}
if(options.h || options.arguments().size()==0) {
    cli.usage()
    return
}

if( options.action == 'generateProperties'){    //<2>
    return generateProperties(options.arguments()[0])
}

if( options.action == 'generateExcel'){ //<3>
    return generateExcel(options.arguments()[0])
}

cli.usage() //<4>
//end::arguments[]

//tag::generateProperties[]
void generateProperties(filename){
    File excelFile = new File(filename)

    String dir = excelFile.absolutePath.split(File.separator).dropRight(1).join(File.separator)
    String name = excelFile.name.split('\\.').dropRight(1).join('.')
    InputStream inp = new FileInputStream(excelFile)

    //<1>
    new File(dir).eachFileMatch ~"${name}(_[A-Za-z]+)?\\.properties", {
        it.delete()
    }

    List<String> languages = []

    Workbook wb = WorkbookFactory.create(inp)
    Sheet sheet = wb.getSheetAt(0)

    sheet.iterator().eachWithIndex{ Row row, int idx-> //<2>

        if( idx == 0){  //<3>
            languages = ['']+row.cellIterator().collect{ '_'+it.stringCellValue }.drop(2)*.toLowerCase()
            return
        }

        String code = row.getCell(0)
        languages.eachWithIndex{ String lang, int i ->  //<4>
            String txt = row.getCell(i+1)?.stringCellValue
            if(txt)
                new File("${name}${lang}.properties") << "$code=$txt\n" //<5>
        }
    }
}
//end::generateProperties[]

//tag::generateExcel[]
void generateExcel(filename){
    File excelFile = new File(filename)

    String dir = excelFile.absolutePath.split(File.separator).dropRight(1).join(File.separator)
    String name = excelFile.name.split('\\.').dropRight(1).join('.')

    Properties defaultProperties = new Properties()
    defaultProperties.load( new File("${name}.properties").newInputStream() ) //<1>

    Map<String,Properties> propertiesMap = [:]  //<2>

    new File(dir).eachFileMatch ~"${name}(_[A-Za-z]+)\\.properties", {  //<3>
        def matcher = (it.name =~ "${name}(_[A-Za-z]+)\\.properties" )
        String lang = matcher[0][1]?.substring(1)
        Properties prp = new Properties()
        prp.load( it.newInputStream() )
        propertiesMap[ lang ] = prp //<4>
    }

    ExcelBuilder.output(new FileOutputStream(new File("${filename}"))) {
        sheet {
            row{
                cell("Code")
                cell("Default")
                propertiesMap.keySet().each { lang ->
                    cell(lang.toUpperCase())
                }
            }
            defaultProperties.propertyNames().each{ String property->   //<5>
                row{
                    cell(property)
                    cell(defaultProperties[property])
                    propertiesMap.keySet().each { lang ->
                        cell(propertiesMap[lang][property])
                    }
                }
            }
        }
    }
}
//end::generateExcel[]
