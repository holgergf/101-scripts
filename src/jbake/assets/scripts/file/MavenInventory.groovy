import java.util.jar.JarEntry
import java.util.jar.JarFile
import groovy.json.JsonOutput

groups = [:]

path = args.length > 0 ? args[0] : System.properties["user.home"]+"/.m2/repository"; //<1>

new File(path).traverse(type: groovy.io.FileType.FILES) { it ->     
    if( it.name.endsWith('.jar')){
        inspectJar(it)
    }
}
println JsonOutput.prettyPrint(new JsonOutput().toJson(groups))  

void inspectJar(File f){
    final JarFile jarFile = new JarFile(f);
    Enumeration<JarEntry> enumOfJar = jarFile.entries()
    JarEntry pom = enumOfJar.find{it.name.endsWith("/pom.properties")}   
    if( pom )
        inspectPom( jarFile, pom )
}

void inspectPom(JarFile jarFile,  JarEntry jarEntry){
    
    InputStream input = jarFile.getInputStream(jarEntry);
    Properties prp = new Properties();
    prp.load(input);   

    def group = groups[prp['groupId']] ?: [:]         
    def artifact = group[prp['artifactId']] ?: [:]
    def version = artifact[prp['version']] ?: []
    
    version << jarFile.name    

    artifact[prp['version']]=version    
    group[prp['artifactId']]=artifact
    groups[prp['groupId']] = group
}
