@Grab("org.yaml:snakeyaml:1.16")
@Grab("com.drewnoakes:metadata-extractor:2.11.0")

import static groovy.io.FileType.FILES

import com.drew.metadata.*
import com.drew.imaging.*
import com.drew.imaging.jpeg.*
import com.drew.metadata.exif.*

dir = new File(args[0])

outdir = new File(args[1])
outdir.mkdirs()

files = []
dir.traverse(type: FILES, maxDepth: 0) { files << it }
files = files.findAll{ file -> 
	String ext = file.name.toLowerCase().split("\\.").last()
	['jpg','png'].contains(ext)
}.sort{ it.lastModified() }

if( !files.size() ){
	println "No files"
	return
}

files.each{ file ->
	String gname = new Date(file.lastModified()).format('yyyy-MM-dd')

	try {
            Metadata metadata = ImageMetadataReader.readMetadata(file)
            metadata.directories.find{ it.name=="Exif IFD0"}.tags.findAll{ 
			it.tagName =="Date/Time"}.each{ tag ->
		gname = tag.description[0..10].replaceAll(":","-")
	    }	    
	}catch( e ){
	    println "error extracting metadata for $file.name : $e.message"
	}

	File newdir = new File(outdir,gname)
	newdir.mkdirs()
println "Move $file.name to $newdir.name"
	file.renameTo( new File(newdir, file.name)  )
}




