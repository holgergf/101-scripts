repoUrl=""
repoSnapUrl=""

void scanDir( dir ){
    dir.eachFile{ f->
        if( f.name.endsWith(".pom") ){  //<1>
            def jar = dir.listFiles().find{
                (it.name.endsWith(".jar") || it.name.endsWith(".war")) &&  //<2>
                        it.name.split('\\.').dropRight(1).join('.') == f.name.split('\\.').dropRight(1).join('.')
            }
            if( jar ){
                String url = jar.name.toLowerCase().contains("-snapshot.") ? repoSnapUrl : repoUrl  //<3>
                """mvn deploy:deploy-file 
                        -Dfile=$jar.absolutePath 
                        -DpomFile=$f.absolutePath 
                        -Durl=$url 
                        -DrepositoryId=$repoId""".execute()  //<4>
            }
        }
    }
    dir.eachDir{ d->  //<5>
        scanDir(d)
    }
}

repoId = args[1]
repoUrl = args[2]
repoSnapUrl = args.length > 3 ? args[3] : args[2]

scanDir( new File(args[0]) )