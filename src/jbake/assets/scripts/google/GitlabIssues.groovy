//tag::dependencies[]
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '1.5.1-dev.20+c11ecb3')
@Grab(group = 'com.puravida.groogle', module = 'groogle-sheet', version = '1.5.1-dev.20+c11ecb3')
@Grab(group='org.gitlab', module='java-gitlab-api', version='4.1.0')

import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.SheetScript
import org.gitlab.api.*
//end::dependencies[]

//tag::arguments[]
String token = args[0]
String projectId = args[1]
String spreadSheetId= args[2]

def api = GitlabAPI.connect("https://gitlab.com",token)
def project = api.getProject(projectId)
//end::arguments[]

SheetScript.instance.with{
    //tag::login[]
    login {
        applicationName '101-groovy'

        withScopes SheetsScopes.SPREADSHEETS

        usingCredentials '/client_secret.json'

        asService true
    }
    //end::login[]

    //tag::dump[]
    withSpreadSheet spreadSheetId, {
        withSheet "$projectId",{
            clearRange 'A', 'AZ'    //<1>
            appendRow "Id", "Title", "Author", "Description" "Created"

            def issuesGroup = api.getIssues(project).findAll{it.state=='opened'}.sort{ a, b->   //<2>
                a.createdAt <=> b.createdAt
            }.collate(20)    //<3>

            issuesGroup.each{ issues ->
                appendRows issues.inject([],{ list, issue ->    //<4>
                    list << [issue.iid, issue.title, issue.author.name, issue.description ?: '', issue.createdAt.toString()]
                    list
                })
            }

        }
    }
    //end::dump[]
}

