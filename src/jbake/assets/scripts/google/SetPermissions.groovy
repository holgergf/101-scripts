//tag::dependencies[]
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grapes([
        @Grab(group = 'com.puravida.groogle', module = 'groogle-drive', version = '1.5.1-dev.32+5d37817')
])
import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveScript
//end::dependencies[]

//tag::dump[]
void dumpFile(fileId){
    drive.with {
        withFile fileId, {
            println "$file.name ($file.id)"
            file.permissions.each { permission ->
                println "\t $permission"
            }
        }
    }
}
//end::dump[]

//tag::dump[]
void dumpPermissions(folderId) {
    def self = this
    drive.with {
        withFiles {
            if(folderId)
                parentId folderId
            eachFile { file, idx ->
                println "$id: $file.name ($file.id)"
                file.permissions.each { permission ->
                    println "\t $permission"
                }
                if( file.mimeType=='application/vnd.google-apps.folder') //<1>
                    self.dumpPermissions(file.id)
            }
        }
    }
}
//end::dump[]

void removeFilePermissions(fileId) {
    drive.with {
        //tag::remove[]
        withFile fileId, { file ->
            permissions {
                strick true
            }
        }
        //end::remove[]
    }
}

void removePermissions(folderId) {
    def self = this
    drive.with {
        withFiles {
            if(folderId)
                parentId folderId

            eachFile { file, id ->
                permissions {
                    strick true
                }
                if( file.mimeType=='application/vnd.google-apps.folder')
                    self.removePermissions(file.id)
            }
        }
    }
}

void shareFilePermissions(fileId, args) {
    drive.with {
        //tag::share[]
        withFile fileId, {
            permissions {
                usersAsReader args

                //others:
                //usersAsWriter args
                //domainAsReader args
                //anyoneAsReader
                //anyoneAsWriter

                strick true
            }
        }
        //end::share[]
    }
}

//tag::main[]
drive = DriveScript.instance    //<1>

void login() {
    drive.with {
        login {
            applicationName 'test-permissions'
            withScopes DriveScopes.DRIVE
            usingCredentials '/client_secret.json'
            asService false     //<2>
        }
    }
}

login() //<3>

switch( args[0] ){  //<4>
    case "dumpFile":
        return dumpFile(args[1])
    case "dumpFolder":
        return dumpPermissions(args.length>1 ?args[1]:null)
    case "removeFolder":
        return removePermissions(args.length>1 ?args[1]:null)
    case "removeFile":
        return removeFilePermissions(args[1])
    case "shareFile":
        return shareFilePermissions(args[1],args[2])
}
//end::main[]
