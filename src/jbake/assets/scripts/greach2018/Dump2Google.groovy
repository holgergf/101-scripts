@GrabConfig(systemClassLoader=true)
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')
@Grab('net.sourceforge.jexcelapi:jxl:2.6.12')
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '1.4.1')
@Grab(group = 'com.puravida.groogle', module = 'groogle-sheet', version = '1.4.1')

import groovy.sql.Sql
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript
import com.google.api.services.sheets.v4.SheetsScopes


GroogleScript.instance.applicationName='101-groovy'
GroogleScript.instance.login(this.class.getResource('client_secret.json').newInputStream(),[SheetsScopes.SPREADSHEETS])
SheetScript.instance.groogleScript=GroogleScript.instance

Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")

SheetScript.instance.withSpreadSheet '1yHt0LcUj4v0lLnaYWfTgYMxvBi7fvdWbT2G7T5hU9C4', {
    withSheet 'Greach2018', { sheet->
        A1 = "ID"
        B1 = "Product"
        C1 = "Sales"
        writeInRange 'A2','C13',{
            sql.eachRow("select * from products order by sales desc limit 3") { row ->
                addRow([row.id,row.name,row.sales])
            }
        }
    }
}

