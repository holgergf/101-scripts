@GrabConfig(systemClassLoader=true)
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')
@Grab(group='org.asciidoctor', module='asciidoctorj', version='1.5.6')
@Grab(group='org.asciidoctor', module='asciidoctorj-pdf', version='1.5.0-alpha.16')
@Grab(group='org.jruby', module='jruby-complete', version='9.1.15.0')

import groovy.sql.Sql
import org.asciidoctor.OptionsBuilder
import org.asciidoctor.AttributesBuilder
import org.asciidoctor.SafeMode
import org.asciidoctor.Asciidoctor.Factory


Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")
txt="""
= Product Catalog 2018

[abstract]
List of our most popular products orders by sales

"""
sql.eachRow("select * from products order by sales desc limit 3") { row ->
    txt="""$txt
== ${row.name} (ID: $row.id)
${row.description} at price ${row.price} has been solded ${row.sales} times
   
    """
}
file=new File('/tmp/TopSales.adoc')
file.text=txt

asciidoctor = Factory.create();
attributes = AttributesBuilder.attributes().
        sectionNumbers(true).
        get()
options = OptionsBuilder.options().
        backend('pdf').
        attributes(attributes).
        safe(SafeMode.UNSAFE).
        get()
asciidoctor.convertFile(file, options)

"firefox /tmp/TopSales.pdf".execute()