//tag::dependencies[]

import groovy.io.FileType

@GrabConfig(systemClassLoader=true)
@Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true)

import static groovyx.javafx.GroovyFX.start
import javafx.application.Platform
import javafx.scene.SnapshotParameters
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
//end::dependencies[]

//tag::calculating[]
bySize=[:]
byNumber=[:]
byBoth=[:]

void scanDir( File dir ){
    dir.eachFile FileType.FILES,{ f->
        split = f.name.split('\\.')
        ext = split.size() ? split.last() : '.'

        bySize."$ext" = (bySize."$ext" ?: 0)+f.size()
        byNumber."$ext" = (byNumber."$ext" ?: 0)+1

        both = byBoth."$ext" ?: [ext,0,0]
        both[1] +=f.size()
        both[2] +=1
        byBoth."$ext" = both
    }
    dir.eachDir{ d ->
        scanDir(d)
    }
}

scanDir(new File(args[0]))

bySize = bySize.sort{ a,b-> b.value<=>a.value }
byNumber = byNumber.sort{ a,b-> b.value<=>a.value }
//end::calculating[]

start { app ->
    //tag::view[]
    stage title: "My Files", visible: true, {
        scene {
            saveNode = stackPane {
                scrollPane {
                    tilePane(padding: 10, prefTileWidth: 480, prefColumns: 2) {

                        pieChart(data: bySize, title: "Size")

                        pieChart(data: byNumber, title: "Number")

                        scatterChart title:'Size vs Number', {
                            byBoth.values().each { bb->
                                series(name: bb[0], data: [bb[1],bb[2]])
                            }
                        }
                    }
                }
            }
        }
    }
    //end::view[]

    //tag::snapshot[]
    def snapshot = saveNode.snapshot(new SnapshotParameters(), null);
    BufferedImage bufferedImage = new BufferedImage(saveNode.width as int, saveNode.height as int, BufferedImage.TYPE_INT_ARGB);
    BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage)

    File file = new File('myfiles.png')
    ImageIO.write(image, "png", file )
    //end::snapshot[]
}
