//tag::dependencies[]
@Grab('com.aestasit.infrastructure.sshoogr:sshoogr:0.9.25')
@GrabConfig(systemClassLoader=true)
//end::dependencies[]

import static com.aestasit.infrastructure.ssh.DefaultSsh.*

options.trustUnknownHosts = true

//tag::config[]
def server_name = "server_1"
def file_config = '/tmp/file.properties'
//end::config[]

//tag::main[]
def read = getFile(file_config,server_name) // <1>

def properties = read.collect{
    if (it.startWith("user.sql")){ // <2>
         return "user.sql=${System.getenv('HOSTNAME')}_changeme"
    }
    return it
}.join('\n')

putFile(file_config,server_name,properties)// <3>
//end::main[]

//tag::write[]
def putFile(file_name,server_name,new_file){
    remoteSession("user:passwd@$server_name:22") {// <1>
        remoteFile(dir).text = properties // <2>
    }
}
//end::write[]

//tag::read[]
def getFile(file_config,server_name){
  remoteSession("user:passwd@$server_name:22") {// <1>
        result =  remoteFile(file_config).text // <2>
  }
  return result.readLines()// <3>
}
//end::read[]
