//tag::dependencies[]
@Grab('org.apache.poi:poi:3.14')
@Grab('org.apache.poi:poi-ooxml:3.14')
//end::dependencies[]

import org.apache.poi.ss.usermodel.*

if (args.length == 0) {
    println "Use:"
    println "   groovy Constact2QRCode.groovy [excel-file]"
    return 1
}

urlRoot = "http://zxing.org/w/chart?cht=qr&chs=350x350&chld=H&choe=UTF-8&chl=";

File f = new File(args[0]);
ByteArrayOutputStream bos

//tag::readExcel[]
WorkbookFactory.create(f,null,false).withCloseable { workbook ->    //<1>
    0.step workbook.getNumberOfSheets(), 1, { sheetNum ->           //<2>
        println "Working on ${workbook.getSheetName(sheetNum)}"
        Sheet sheet = workbook.sheets[sheetNum]
        for( Row row : sheet){
            if( row.rowNum ){                                       //<3>
                row.createCell(8).cellValue = generateQRLink(row) //<4>
            }
        }
    }
    bos = new ByteArrayOutputStream()
    workbook.write(bos)                                             //<5>
}
f << bos.toByteArray()                                              //<6>
//end::readExcel[]

//tag::dumpQr[]
if( args.length > 1 ){
    File dumpFolder = new File(args[1])
    dumpFolder.mkdirs()
    WorkbookFactory.create(f,null,true).withCloseable { workbook ->     //<1>
        0.step workbook.getNumberOfSheets(), 1, { sheetNum ->
            println "Dumping on ${workbook.getSheetName(sheetNum)}"
            Sheet sheet = workbook.sheets[sheetNum]
            for( Row row : sheet){
                if( row.rowNum ){
                    new File(dumpFolder,"${row.getCell(0)}.png").withOutputStream {
                        it.bytes = "${row.getCell(8)}".toURL().bytes    //<2>
                    }
                }
            }
        }
    }
}
//end::dumpQr[]

//tag::generateVCard[]
String generateVCard( name, title , tlf , email, addr, organization, url){
    String getvcard = "BEGIN%3AVCARD%0AVERSION%3A3.0";
    if(name) getvcard += "%0AN%3A"+URLEncoder.encode(name,"UTF-8")      //<1>
    if(organization) getvcard += "%0AORG%3A"+URLEncoder.encode(organization,"UTF-8")
    if(title) getvcard += "%0ATITLE%3A"+URLEncoder.encode(title,"UTF-8")
    if(tlf) getvcard += "%0ATEL%3A"+URLEncoder.encode(tlf,"UTF-8");
    if(email) getvcard += "%0AEMAIL%3A"+URLEncoder.encode(email,"UTF-8");
    if(addr) getvcard += "%0AADR%3A"+URLEncoder.encode(addr,"UTF-8")
    if(url) getvcard += "%0AURL%3A"+URLEncoder.encode(url,"UTF-8");
    getvcard += "%0AEND%3AVCARD";
    return getvcard;
}
//end::generateVCard[]

//tag::generateQRLink[]
String generateQRLink(Row range){
    int col=1
    String name = "${range.getCell(col++)}"
    String title = "${range.getCell(col++)}"
    String tlf= "${range.getCell(col++)}"
    String email="${range.getCell(col++)}"
    String addr="${range.getCell(col++)}"
    String organization="${range.getCell(col++)}"
    String url="${range.getCell(col++)}"

    String vcard = urlRoot + generateVCard(name, title , tlf , email, addr, organization, url)
    vcard
}
//end::generateQRLink[]

