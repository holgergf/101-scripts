= From properties to YML
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-03-07
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-lang: es
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/ConfigScript.groovy
:idprefix:
:imagesdir: ../images
:jbake-spanish: config_script
:jbake-lang: gb
endif::[]

When your script is easy and require a few params probably parse the command line it's enougth to you, using for
example _CliBuilder_. As your application grown your requirements grown and you delegate to a _properties_ file.

Although _properties_ it's a common option it's easy to make mistakes. In this post we'll see how easy is to use
a format more readable for humans and powerfull than _properties_ called YAML https://es.wikipedia.org/wiki/YAML

With this format we can write key values but also arrays, maps and so on. This is an example when we configure
two datasources and one array of maps:

[source,yml]
----
include::{sourcedir}/scripts/basico/config_script.yml[]
----

The script will read the file and if they are some mistakes the parser will be throw an exception.
Once the YML is loaded we can traverse the configuration using tipical groovy operators:

- check if a key it's present with the '?' operator
- traverse across an array using _each_ or _eachWithIndex_

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----




include::{contentdir}/include-source.txt[]
