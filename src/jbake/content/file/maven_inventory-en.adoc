= Maven Inventory
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2019-01-28
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, maven, pom, jarfile
:jbake-category: file
:jbake-script: /scripts/file/MavenInventory.groovy
:jbake-spanish: maven_inventory
:jbake-lang: gb
:idprefix:
:imagesdir: ../images
endif::[]

A lot of libraries are built with maven and most of of them include 
a Manifest directory with a pom.properties with the details of the build 
(as the group, artifact and version)

Sometimes we have a directory with a lot of these libraries and it can happen 
we have duplicated some of them in different directories or worst we mix differents 
versions of the same artefact and we have classloader issues

With this little script we can do a quickly scan of these artifacts an generate a report with all this information.

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> if no path is provided we'll use the typical .m2 directory


- We iterate over all files (ending with `.jar`) of a directory recursively.
- We inspect each file searching if contains the `pom.properties` file into it
- We maintain a Map (groups) of Map (artifacts) of List (versions) and store in this last the path of the file


At the end of the scan we dump into the console the Map in JSON format but you can use other format if you preffer.

== Result

This is an example of the console:

[source,json]
----
  "org.apache.maven.shared": {
        "maven-shared-utils": {
            "0.1": [
                ".m2/repository/org/apache/maven/shared/maven-shared-utils/0.1/maven-shared-utils-0.1.jar"
            ],
            "0.3": [
                ".m2/repository/org/apache/maven/shared/maven-shared-utils/0.3/maven-shared-utils-0.3.jar"
            ]
        },
        "maven-shared-incremental": {
            "1.1": [
                ".m2/repository/org/apache/maven/shared/maven-shared-incremental/1.1/maven-shared-incremental-1.1.jar"
            ]
        },
        "maven-repository-builder": {
            "1.0-alpha-2": [
                ".m2/repository/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.jar"
            ]
        },
        "maven-invoker": {
            "2.1.1": [
                ".m2/repository/org/apache/maven/shared/maven-invoker/2.1.1/maven-invoker-2.1.1.jar"
            ],
            "2.2": [
                ".m2/repository/org/apache/maven/shared/maven-invoker/2.2/maven-invoker-2.2.jar"
            ]
        },
        "maven-plugin-testing-harness": {
            "1.1": [
                ".m2/repository/org/apache/maven/shared/maven-plugin-testing-harness/1.1/maven-plugin-testing-harness-1.1.jar"
            ]
        },
        "file-management": {
            "1.2.1": [
                ".m2/repository/org/apache/maven/shared/file-management/1.2.1/file-management-1.2.1.jar"
            ],
            "1.1": [
                ".m2/repository/org/apache/maven/shared/file-management/1.1/file-management-1.1.jar"
            ]
        },
        "maven-shared-io": {
            "1.1": [
                ".m2/repository/org/apache/maven/shared/maven-shared-io/1.1/maven-shared-io-1.1.jar"
            ]
        },
----
