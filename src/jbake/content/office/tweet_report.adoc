= TweetReport (Persistencia con SQLite)
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-12-09
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: office
:jbake-script: /scripts/office/TweetReport.groovy
:idprefix:
:imagesdir: ../images
endif::[]



En este post vamos a analizar un caso de negocio que seguramente no ocurre muy a menudo pero que espero nos ayude
a resolver situaciones parecidas. En este caso vamos a desarrollar un script que se podrá ejecutar repetidas veces
a lo largo del tiempo (cada hora, varias veces al día, etc) cuya lógica de negocio queremos que se mantenga durante
la ejecución de las mismas.

Nuestro Community Manager siente curiosidad por ciertos usuarios de Twitter que nos siguen en nuestra cuenta oficial
y quiere hacer un pequeño estudio sobre los mismos que va a durar un cierto tiempo (dias/semanas/meses). Durante este
tiempo nos irá proporcionando usuarios de Twitter de los que tendremos que obtener ciertos datos públicos e ir
guardándolos. Así mismo durante este período nos irá pidiendo que le hagamos un pequeño informe con los datos guardados.

En un escenario convencional, podríamos optar por escribir en un fichero plano la información de cada ejecución
o si queremos algo más robusto instalaríamos un motor de base de datos tipo MySQL, Postgre, SQLServer, etc. Mientras
que lo primero es muy simple de mantener resulta muy complejo de gestionar/programar. Por el contrario la segunda
opción es mucho más robusta pero más compleja.

A medio de camino de ambas soluciones contamos con proyectos como *SQLite* que nos permitirán crear y acceder a los
datos mediante un acceso JDBC (base de datos) pero sin las complejidades de tener que instalar y mantener un motor
de datos más completo.

== Dependencias

Para este scritp necesitaremos acceder a Twitter y a una base de datos SQLite por lo que definimos las dependencias:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----


== Argumentos

El script admitirá la siguiente lista de argumentos:

- --initialize, recrea la base de datos
- --username, busca un usuario proporcionado por parámetro y lo incluye en la base de datos
- --all, recorre todos los usuarios existentes en la base de datos y los actualiza
- --report, genera un informe con los datos guardados hasta la fecha

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=cli]
----

== Modelo

Para ayudar en la encapsulación de los datos definimos una clase _TwitterUser_ la cual se auto-actualiza si usamos el
constructor que proporciona un id de Twitter:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=model]
----

== Prepare

Antes de realizar ninguna acción contra la base de datos el propio script realiza una preparación de la misma. Si
se indicó el parámetro *--initialize* realizará un borrado de la base de datos. En cualquier caso creará la tabla
si no existe

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=prepare]
----

== Update User

Si se indica un usuario, el script creará un objeto de la clase _TwitterUser_ y mediante
una simple select por su id determinará si existe ya. Si existe se procederá a actualizar el registro mientras
que si no existe se insertará un registro nuevo

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=update]
----

Mediante la opción *--all* hacemos que el script recorra todos los usuarios guardados hasta la fecha e invoque el método
*upate* para cada uno de ellos


== Report User

En cualquier momento podemos solicitar que el script genere un informe con el estado de la base de datos, lo cual
en nuestro ejemplo consistirá en recorrer los registros y mostrarlos por consola:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=report]
----

== Persistencia

Al ejecutar el script se creará un fichero *tweetreport.db* (indicado en la cadena de conexión Sql) que SQLite
utilizará para ofrecer el servicio de persistencia.

De esta forma simple podremos dotar a nuestros scritps de la capacidad de tener una base de datos sencilla y sin
complejidades de instalación ni administración


include::{contentdir}/include-source.txt[]
